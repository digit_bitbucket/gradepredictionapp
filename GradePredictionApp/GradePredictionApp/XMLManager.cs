﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using GradePredictionApp.Models;

namespace GradePredictionApp
{
    public static class XMLManager
    {

        public static Boolean write()
        {
            try
            {
                Course course = GlobalVariableHolder.course;

                System.Xml.Serialization.XmlSerializer writer = new System.Xml.Serialization.XmlSerializer(typeof(Course));

                // Creating the path to the file
                var path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "//GradePredictionAppData.xml";

                System.IO.FileStream file = System.IO.File.Create(path);

                writer.Serialize(file, course);
                file.Close();
                return true;
            } catch (Exception ex) {                
            }
            return false;
        }

        public static Boolean Read()       {
            try
            {
                System.Xml.Serialization.XmlSerializer reader = new System.Xml.Serialization.XmlSerializer(typeof(Course));

                // Creating the path to the file
                var path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "//GradePredictionAppData.xml";
                System.IO.StreamReader file = new System.IO.StreamReader(path);

                Course course = (Course)reader.Deserialize(file);
                GlobalVariableHolder.course = course;

                file.Close();
                return true;
            }
            catch (Exception ex)
            {
            }
            return false;        
        }


    }
}
