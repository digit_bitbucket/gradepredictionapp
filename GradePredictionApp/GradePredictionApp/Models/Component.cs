﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GradePredictionApp.Models
{
    
    [System.Serializable]

    public class Component
    {

        public String componentName;
        public int componentWeight;
        public int componentMark;

        public Component(String componentName, int componentWeight)
        {
            this.componentName = componentName;
            this.componentWeight = componentWeight;
            this.componentMark = 0;
        }

        public Component()
        {

        }

        public String getComponentName()
        {
            return componentName;
        }

        public int getComponentWeight()
        {
            return componentWeight;
        }

        public int getComponentMark()
        {
            return componentMark;
        }

        public void setComponentMark(int componentMark){
            this.componentMark = componentMark;
        }


    }
}
