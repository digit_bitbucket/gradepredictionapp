﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GradePredictionApp.Models
{
    
    [System.Serializable]

    public class Course
    {

        public String courseName;
        public String studentName;
        public Level level_4;
        public Level level_5;
        public Level level_6;
        public int courseMark;
        public String courseGrade;

        public Course(String studentName, String courseName)
        {
            this.courseName = courseName;
            this.studentName = studentName;
            level_4 = new Level(4);
            level_5 = new Level(4);
            level_6 = new Level(5);
            courseMark = 0;
            courseGrade = "N/A";
        }

        public Course()
        {

        }

        public String getCourseName()
        {
            return courseName;
        }


        public Level getLevel_4()
        {
            return level_4;
        }
        
        public Level getLevel_5()
        {
            return level_5;
        }

        public Level getLevel_6()
        {
            return level_6;
        }

        public int getCourseMark()
        {
            return courseMark;
        }

        public String getCourseGrade()
        {
            return courseGrade;
        }

        public String getStudentName()
        {
            return studentName;
        }

        public void setCourseMark(int courseMark)
        {
            this.courseMark = courseMark;
        }

        public void setCourseGrade(String courseGrade)
        {
            this.courseGrade = courseGrade;
        }

    }
}
