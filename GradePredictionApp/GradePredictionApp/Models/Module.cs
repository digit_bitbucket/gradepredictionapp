﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GradePredictionApp.Models
{

    [System.Serializable]

    public class Module
    {

        public String moduleName;
        public String moduleId;
        public int moduleCreditLevel;
        public List<Component> componentList;
        public int moduleMark;

        public Module(String moduleId, String moduleName, int moduleCreditLevel, List<Component> componentList)
        {
            this.moduleId = moduleId;
            this.moduleName = moduleName;
            this.moduleCreditLevel = moduleCreditLevel;
            this.componentList = componentList;
            moduleMark = 0;
        }

        public Module()
        {

        }

        public String getModuleName()
        {
            return moduleName;
        }
        public String getModuleId()
        {
            return moduleId;
        }
        public int getModuleCreditLevel()
        {
            return moduleCreditLevel;
        }
        public List<Component> getComponentArray()
        {
            return componentList;
        }
        public int getModuleMark()
        {
            return moduleMark;
        }

        public Component getComponentFromName(String componentName){
            if (componentList != null)
            {
                foreach (var currentComponent in componentList)
                {
                    if (currentComponent.getComponentName().Equals(componentName))
                    {
                        return currentComponent;
                    }
                }
            }
            return null;
        }

        public void setModuleMark(int moduleMark)
        {
            this.moduleMark = moduleMark;
        }
    }
}
