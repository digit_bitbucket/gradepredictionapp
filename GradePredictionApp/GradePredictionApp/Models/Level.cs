﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GradePredictionApp.Models
{
    
    [System.Serializable]

    public class Level
    {

        public int levelNumber;
        public List<Module> moduleList;
        public int levelMark;
        public int totalCredits;

        public Level(int levelNumber)
        {
            this.levelNumber = levelNumber;
        }

        public Level()
        {           
        }

        public int getLevelNumber()
        {
            return levelNumber;
        }

        public List<Module> getModuleArray()
        {
            return moduleList;
        }
        public int getLevelMark()
        {
            return levelMark;
        }
        public int getTotalCredits()
        {
            return totalCredits;
        }

        public Boolean checkIfModuleIdExistsInLevel(String moduleId)
        {
            if (moduleList != null)
            {
                if (moduleList.Count > 0)
                {
                    foreach (var currentModule in moduleList)
                    {
                        if (currentModule.getModuleId() == moduleId)
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        public Boolean checkIfModuleNameExistsInLevel(String moduleName)
        {
            if (moduleList != null)
            {
                if (moduleList.Count > 0)
                {
                    foreach (var currentModule in moduleList)
                    {
                        if (currentModule.getModuleName().ToString().Equals(moduleName))
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        public void AddModule(Module newModule)
        {
            if (moduleList == null)
            {
                moduleList = new List<Module>();
            }
            moduleList.Add(newModule);           
        }


        public int getTotalModuleCredits()
        {
            if (moduleList == null)
            {                
                return 0;
            }
            else
            {
                int totalCredits = 0;
                foreach (var currentModule in moduleList){
                    totalCredits = totalCredits + currentModule.getModuleCreditLevel();
                }
                return totalCredits;
            }
        }


        public void setLevelMark(int levelMark)
        {
            this.levelMark = levelMark;
        }
    }
}
