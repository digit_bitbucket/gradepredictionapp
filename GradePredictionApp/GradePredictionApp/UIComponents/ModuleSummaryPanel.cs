﻿using GradePredictionApp.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GradePredictionApp.UIComponents
{
    class ModuleSummaryPanel : TableLayoutPanel
    {

        private Label lblModuleCode;
        private Label lblModuleCodeValue;
        private Label lblModuleName;
        private Label lblModuleNameValue;
        private Label lblCreditLevel;
        private Label lblCreditLevelValue;
        private Label lblModuleMark;
        private Label lblModuleMarkValue;

        private Panel moduleSummaryPanelHolder;
        private ModuleComponentDetailsPanel moduleDetailsPanel;

        private Module selectedModule;


        public ModuleSummaryPanel(Size size, Module selectedModule)
        {
            this.BackColor = Color.DarkGoldenrod;
            this.Size = size;
            this.selectedModule = selectedModule;

            createUi();

            // Setting the rows and columns of the table
            this.ColumnCount = 2;
            this.RowCount = 5;
        }


        private void createUi()
        {
            lblModuleCode = new Label();
            lblModuleCodeValue = new Label();
            lblModuleName = new Label();
            lblModuleNameValue = new Label();
            lblCreditLevel = new Label();
            lblCreditLevelValue = new Label();
            lblModuleMark = new Label();
            lblModuleMarkValue = new Label();

            lblModuleCode.Text = "MODULE CODE :";
            lblModuleCodeValue.Text = selectedModule.getModuleId();
            lblModuleName.Text = "MODULE NAME :";
            lblModuleNameValue.Text = selectedModule.getModuleName();
            lblCreditLevel.Text = "CREDIT LEVEL :";
            lblCreditLevelValue.Text = selectedModule.getModuleCreditLevel() + " %";
            lblModuleMark.Text = "MODULE MARKS :";
            lblModuleMarkValue.Text = selectedModule.getModuleMark() + " %";

            // Setting the margin in the components 
            lblModuleCode.Margin = new Padding(6, 8, 0, 0);
            lblModuleCodeValue.Margin = new Padding(6, 8, 0, 0);
            lblModuleName.Margin = new Padding(6, 8, 0, 0);
            lblModuleNameValue.Margin = new Padding(6, 8, 0, 0);
            lblCreditLevel.Margin = new Padding(6, 8, 0, 0);
            lblCreditLevelValue.Margin = new Padding(6, 8, 0, 0);
            lblModuleMark.Margin = new Padding(6, 8, 0, 0);
            lblModuleMarkValue.Margin = new Padding(6, 8, 0, 0);

            // Creating a panel to hold the module component details panel
            moduleSummaryPanelHolder = new Panel();
            moduleSummaryPanelHolder.BackColor = Color.DarkGoldenrod;

            moduleDetailsPanel = new ModuleComponentDetailsPanel(moduleSummaryPanelHolder.Size, selectedModule.getComponentArray());

            this.Controls.Add(lblModuleCode, 0, 0);
            this.Controls.Add(lblModuleCodeValue, 1, 0);
            this.Controls.Add(lblModuleName, 0, 1);
            this.Controls.Add(lblModuleNameValue, 1, 1);
            this.Controls.Add(lblCreditLevel, 0, 2);
            this.Controls.Add(lblCreditLevelValue, 1, 2);
            this.Controls.Add(lblModuleMark, 0, 3);
            this.Controls.Add(lblModuleMarkValue, 1, 3);
            this.Controls.Add(moduleSummaryPanelHolder, 0, 4);
            this.SetColumnSpan(moduleSummaryPanelHolder, 2);

            moduleSummaryPanelHolder.Controls.Clear();
            moduleSummaryPanelHolder.Controls.Add(moduleDetailsPanel);

            setComponentSizes(this.Size);
        }

        // Stiing the sizes of components
        public void setComponentSizes(Size size)
        {
            this.Size = size;

            lblModuleCode.Width = (this.Width / 2) - 15;
            lblModuleCodeValue.Width = (this.Width / 2) - 15;
            lblModuleName.Width = (this.Width / 2) - 15;
            lblModuleNameValue.Width = (this.Width / 2) - 15;
            lblCreditLevel.Width = (this.Width / 2) - 15;
            lblCreditLevelValue.Width = (this.Width / 2) - 15;
            lblModuleMark.Width = (this.Width / 2) - 15;
            lblModuleMarkValue.Width = (this.Width / 2) - 15;
            moduleSummaryPanelHolder.Width = this.Width - 10;

            lblModuleCode.Height = (this.Height / 7);
            lblModuleCodeValue.Height = (this.Height / 7);
            lblModuleName.Height = (this.Height / 7);
            lblModuleNameValue.Height = (this.Height / 7);
            lblCreditLevel.Height = (this.Height / 7);
            lblCreditLevelValue.Height = (this.Height / 7);
            lblModuleMark.Height = (this.Height / 7);
            lblModuleMarkValue.Height = (this.Height / 7);
            moduleSummaryPanelHolder.Height = ((this.Height / 7) * 3) - 40;

            if (moduleDetailsPanel != null)
            {
                moduleDetailsPanel.setComponentSizes(moduleSummaryPanelHolder.Size);
            }
                
        }


        public Module getSelectedModule()
        {
            return selectedModule;
        }


    }
}
