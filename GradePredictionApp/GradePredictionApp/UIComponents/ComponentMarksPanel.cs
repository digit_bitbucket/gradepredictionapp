﻿using GradePredictionApp.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GradePredictionApp.UIComponents
{
    class ComponentMarksPanel : TableLayoutPanel
    {

        private Label lblComponentName;
        private Label lblComponentNameValue;
        private Label lblMarks;
        private Label lblWeight;
        private Label lblWeightValue;

        private TrackBar marksTrackBar;

        private Button btnDone;

        private Component selectedComponent;
        private Module selectedModule;


        public ComponentMarksPanel(Size size, Component selectedComponent, Module selectedModule)
        {

            this.Size = size;
            this.selectedComponent = selectedComponent;
            this.selectedModule = selectedModule;
            this.BackColor = Color.Firebrick;            

            // Sets padding to make the panel scrollable vertically
            Padding p = this.Padding;
            this.Padding = new Padding( p.Left, p.Top, SystemInformation.VerticalScrollBarWidth, p.Bottom);
                      
            createUi();

            // More settings on the verticle scrollability
            this.Dock = DockStyle.Right;
            this.AutoScroll = true;

            setComponentSizes(this.Size);

        }

        private void createUi()
        {
            // The columns and rows of the table layout
            this.ColumnCount = 2;
            this.RowCount = 4;
            
            lblComponentName = new Label();
            lblComponentNameValue = new Label();
            lblMarks = new Label();
            lblWeight = new Label();
            lblWeightValue = new Label();

            lblComponentName.Text = "COMPONENT NAME :";
            lblComponentName.TextAlign = ContentAlignment.MiddleLeft;

            lblComponentNameValue.Text = selectedComponent.getComponentName();
            lblComponentNameValue.Margin = new Padding(9,0,0,0);
            lblComponentNameValue.TextAlign = ContentAlignment.MiddleLeft;

            lblMarks.Text = "MARKS : [" + selectedComponent.getComponentMark() + " %]";
            lblMarks.TextAlign = ContentAlignment.MiddleLeft;

            lblWeight.Text = "WEIGHT :";
            lblWeight.TextAlign = ContentAlignment.MiddleLeft;

            lblWeightValue.Text = selectedComponent.getComponentWeight() + " %";
            lblWeightValue.TextAlign = ContentAlignment.MiddleLeft;
            lblWeightValue.Margin = new Padding(9, 0, 0, 0);

            marksTrackBar = new TrackBar();
            marksTrackBar.Maximum = 100;
            marksTrackBar.Minimum = 0;
            marksTrackBar.TickFrequency = 10;
            marksTrackBar.Margin = new Padding(0, 17, 0, 0);
            marksTrackBar.Value = selectedComponent.getComponentMark();

            // Event handling when a value change happens in the track bar
            marksTrackBar.ValueChanged += delegate
            {
                lblMarks.Text = "MARKS : [" + marksTrackBar.Value + " %]";
            };

            btnDone = new Button();
            btnDone.Text = "DONE";
            btnDone.Margin = new Padding(11, 11, 0, 11);
            btnDone.Click  += delegate
            {
                // Insert the component mark into the objects in the memory
                BuisnessLogic.setComponentMark(selectedComponent, marksTrackBar.Value, selectedModule);
                MessageBox.Show("Memory updated!!! :D", "Changes updated");
            };

            // Adding controls to the panel
            this.Controls.Add(lblComponentName, 0, 0);
            this.Controls.Add(lblComponentNameValue, 1, 0);
            this.Controls.Add(lblMarks, 0, 1);
            this.Controls.Add(marksTrackBar, 1, 1);
            this.Controls.Add(lblWeight, 0, 2);
            this.Controls.Add(lblWeightValue, 1, 2);
            this.Controls.Add(btnDone, 1, 3);

            setComponentSizes(this.Size);
            
            lblComponentName.Height = (int) (this.Height / 4.5);
            lblMarks.Height = (int)(this.Height / 4.5);
            lblWeight.Height = (int)(this.Height / 4.5);

            lblComponentNameValue.Height = (int)(this.Height / 4.5);
            lblWeightValue.Height = (int)(this.Height / 4.5);
            marksTrackBar.Height = (int)(this.Height / 4.5);
            btnDone.Height = (int) (this.Height / 4.5);
        }

        // Setting sizes in the panel
        public void setComponentSizes(Size size)
        {
            this.Size = size;

            lblComponentName.Width = (this.Width / 4) * 1;
            lblMarks.Width = (this.Width / 4) * 1;
            lblWeight.Width = (this.Width / 4) * 1;

            lblComponentNameValue.Width = (int)((this.Width / 4) * 2.5);
            lblWeightValue.Width = (int)((this.Width / 4) * 2.5);
            marksTrackBar.Width = (int)((this.Width / 4) * 2.5);
            btnDone.Width = (int)(((this.Width / 4) * 2.5) - 25);

        }
    }
}
