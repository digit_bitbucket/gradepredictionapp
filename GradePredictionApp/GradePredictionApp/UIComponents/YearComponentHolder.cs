﻿using GradePredictionApp.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GradePredictionApp.UIComponents
{
    class YearComponentHolder : FlowLayoutPanel
    {

        List<Button> buttonArray;
        Button moduleSummaryButton;
        int numberOfButtons;
        LevelTabPage parentPage;

        public YearComponentHolder(Size size, LevelTabPage parentPage)
        {
            this.BackColor = Color.Gray;
            this.parentPage = parentPage;
            this.Size = size;

            // Button holding list
            buttonArray = new List<Button>();
            numberOfButtons = 0;

            // Setting horizontal scrollablility
            this.FlowDirection = System.Windows.Forms.FlowDirection.LeftToRight;
            this.AutoScroll = true;
            this.WrapContents = false;

        }

 

        public void addModuleControls(Module insertingModule)
        {
            this.Controls.Clear();

            // Adding a button with module name and id
            Button moduleDetails = new Button();
            moduleDetails.BackColor = Color.YellowGreen;
            moduleDetails.Text = (" " + insertingModule.getModuleId() + " : " + insertingModule.getModuleName());
            this.Controls.Add(moduleDetails);

            if (this.Height > 20)
            {
                moduleDetails.Height = this.Height - 20;
                moduleDetails.Width = 110;

                // Iterating to find every component in the module list
                foreach (var currentComponent in insertingModule.getComponentArray())
                {

                    // Adding a dynamic to every component
                    Button newButton = new Button();
                    newButton.Text = (currentComponent.getComponentName() + "  [" + currentComponent.getComponentWeight() + "%]");
                    this.Controls.Add(newButton);
                    if (this.Height > 20)
                    {
                        newButton.Height = this.Height - 20;
                    }
                    newButton.Width = 110;
                    newButton.Click += delegate
                    {
                        // Sets the component details to the mddle panel of the parent form
                        parentPage.setComponentDetailsPanel(insertingModule.getModuleName(), currentComponent.getComponentName());                  
                    };
                }

                // Inserting the module summary button
                Button moduleSummary = new Button();
                moduleSummary.Text = ("SUMMARY");
                this.Controls.Add(moduleSummary);
                if (this.Height > 20)
                {
                    moduleSummary.Height = this.Height - 20;
                }
                moduleSummary.Width = 110;
                moduleSummary.Click += delegate
                {
                    // Displays the sumary of module in the middle panel
                    parentPage.displayModuleSummary(insertingModule.getModuleName());
                };
            }
        }

 
        // Sets the sizes of components
        public void setComponentButtonSizes(Size size)
        {
            this.Size = size;
            for (int i = 0; i < numberOfButtons; i++)
            {
                if ((buttonArray[i] != null) && (this.Height > 20))
                {
                    buttonArray[i].Height = this.Height - 20;
                }
            }

            if ((moduleSummaryButton != null) && (this.Height > 20))
            {
                moduleSummaryButton.Height = this.Height - 20;
            }
        }
               

    }
}
