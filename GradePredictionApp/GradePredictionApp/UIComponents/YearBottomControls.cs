﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GradePredictionApp.UIComponents
{
    class YearBottomControls : FlowLayoutPanel
    {

        LevelTabPage parentTabPage;
        Button btnSave;
        Button btnExit;
        Button btnAddModule;
        Button btnRemove;

        public YearBottomControls(Size size, LevelTabPage parentTabPage)
        {
            this.parentTabPage = parentTabPage;
            this.Size = size;          
            this.BackColor = Color.Yellow;

            // Setting the horizontal scrollability
            this.FlowDirection = System.Windows.Forms.FlowDirection.LeftToRight;
            insertButtons();            
        }

        // Insering the 4 main command buttons
        private void insertButtons()
        {
            btnAddModule = new Button();
            btnAddModule.Text = "ADD MODULE";
            addButton(btnAddModule);
            btnAddModule.Click += new EventHandler(btnAddModule_Click);

            btnRemove = new Button();
            btnRemove.Text = "REMOVE MODULE";
            addButton(btnRemove);
            btnRemove.Click += new EventHandler(btnRemove_Click);

            btnSave = new Button();
            btnSave.Text = "SAVE DATA";
            addButton(btnSave);
            btnSave.Click += new EventHandler(btnSaveData_Click);

            btnExit = new Button();
            btnExit.Text = "EXIT";
            addButton(btnExit);
            btnExit.Click += new EventHandler(btnExit_Click);
        }

        // Adds a button
        private void addButton(Button tempButton)
        {
            setButtonSize(tempButton);
            this.Controls.Add(tempButton);

        }

        // Sets sizes of components
        public void setComponentSizes(Size size)
        {
            this.Size = size;
            setButtonSize(btnSave);
            setButtonSize(btnExit);
            setButtonSize(btnAddModule);
            setButtonSize(btnRemove);
        }

        // Sets the sizes of buttons
        private void setButtonSize(Button tempButton)
        {
            if (this.Height > 70)
            {
                tempButton.Height = this.Height - 70;
            }
            if (this.Width > 60)
            {
                tempButton.Width = (this.Width / 4) - 13;
            }       
        }
        

        private void btnRemove_Click(object sender, EventArgs e)
        {
            // Removes the button using a method in the parent tab panel
            parentTabPage.deleteModule();            
        }

        private void btnAddModule_Click(object sender, EventArgs e)
        {    
            // Displays the module insertion screen
            AddModuleScreen insertionScreen = new AddModuleScreen(parentTabPage);

            // Dieplay the screen as a dialog
            insertionScreen.ShowDialog();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnSaveData_Click(object sender, EventArgs e)
        {
            // Save data into the XML file. Checks the success of the  operation
            XMLManager.write();
            if (XMLManager.write())
            {
                MessageBox.Show("Operation succesful!!! :D :D", "Data Saved");
            }
            else
            {
                MessageBox.Show("Operation failed!!! :( :(", "Saving Failed");
            }
        }

    }

}
