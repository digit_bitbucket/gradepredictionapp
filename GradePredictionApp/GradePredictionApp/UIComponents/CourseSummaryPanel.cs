﻿using GradePredictionApp.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GradePredictionApp.UIComponents
{
    class CourseSummaryPanel : TableLayoutPanel
    {

        private Label lblStudentName;
        private Label lblStudentNameValue;
        private Label lblCourseName;
        private Label lblCourseNameValue;
        private Label lblLevel_4Marks;
        private Label lblLevel_4MarksValue;
        private Label lblLevel_5Marks;
        private Label lblLevel_5MarksValue;
        private Label lblLevel_6Marks;
        private Label lblLevel_6MarksValue;
        private Label lblTotalAverage;
        private Label lblTotalAverageValue;
        private Label lblGrade;
        private Label lblGradeValue;

        private Course course;

        public CourseSummaryPanel(Size size)
        {
            this.BackColor = Color.YellowGreen;
            this.Size = size;

            // Defining the rows and column numbers of the table
            this.ColumnCount = 2;
            this.RowCount = 7;

            Padding p = this.Padding;
            this.Padding = new Padding(p.Left, p.Top, SystemInformation.VerticalScrollBarWidth, p.Bottom);

            // Setting the verticle scroll property
            this.Dock = DockStyle.Left | DockStyle.Top;
            this.AutoScroll = true;
            this.AutoScrollMinSize = new Size(100,100);

            this.course = GlobalVariableHolder.course;

            createUi();

            setComponentSizes(this.Size);
        }

        private void createUi()
        {
            lblStudentName = new Label();
            lblStudentNameValue = new Label();
            lblCourseName = new Label();
            lblCourseNameValue = new Label();
            lblLevel_4Marks = new Label();
            lblLevel_4MarksValue = new Label();
            lblLevel_5Marks = new Label();
            lblLevel_5MarksValue = new Label();
            lblLevel_6Marks = new Label();
            lblLevel_6MarksValue = new Label();
            lblTotalAverage = new Label();
            lblTotalAverageValue = new Label();
            lblGrade = new Label();
            lblGradeValue = new Label();


            lblStudentName.Text = "STUDENT NAME :";
            lblStudentNameValue.Text = course.getStudentName();
            lblCourseName.Text = "COURSE NAME :";
            lblCourseNameValue.Text = course.getCourseName();
            lblLevel_4Marks.Text = "LEVEL 4 AVERAGE :";
            lblLevel_4MarksValue.Text = course.getLevel_4().getLevelMark() + " %";
            lblLevel_5Marks.Text = "LEVEL 5 AVERAGE :";
            lblLevel_5MarksValue.Text = course.getLevel_5().getLevelMark() + " %";
            lblLevel_6Marks.Text = "LEVEL 6 AVERAGE :";
            lblLevel_6MarksValue.Text = course.getLevel_6().getLevelMark() + " %";
            lblTotalAverage.Text = "COURSE AVERAGE :";
            lblTotalAverageValue.Text = course.getCourseMark() + " %";
            lblGrade.Text = "GRADE :";
            lblGradeValue.Text = course.getCourseGrade();
            
            // Displaying the backcolor according to the marks
            this.BackColor = BuisnessLogic.getColor(course.getCourseMark());

            // Making the text align to the middle left
            lblStudentName.TextAlign = ContentAlignment.MiddleLeft;
            lblStudentNameValue.TextAlign = ContentAlignment.MiddleLeft;
            lblCourseName.TextAlign = ContentAlignment.MiddleLeft;
            lblCourseNameValue.TextAlign = ContentAlignment.MiddleLeft;
            lblLevel_4Marks.TextAlign = ContentAlignment.MiddleLeft;
            lblLevel_4MarksValue.TextAlign = ContentAlignment.MiddleLeft;
            lblLevel_5Marks.TextAlign = ContentAlignment.MiddleLeft;
            lblLevel_5MarksValue.TextAlign = ContentAlignment.MiddleLeft;
            lblLevel_6Marks.TextAlign = ContentAlignment.MiddleLeft;
            lblLevel_6MarksValue.TextAlign = ContentAlignment.MiddleLeft;
            lblTotalAverage.TextAlign = ContentAlignment.MiddleLeft;
            lblTotalAverageValue.TextAlign = ContentAlignment.MiddleLeft;
            lblGrade.TextAlign = ContentAlignment.MiddleLeft;
            lblGradeValue.TextAlign = ContentAlignment.MiddleLeft;
                        
            // Inserting the margins
            lblStudentName.Margin = new Padding(26, 28, 0, 0);
            lblStudentNameValue.Margin = new Padding(6, 28, 0, 0);
            lblCourseName.Margin = new Padding(26, 28, 0, 0);
            lblCourseNameValue.Margin = new Padding(6, 28, 0, 0);
            lblLevel_4Marks.Margin = new Padding(26, 28, 0, 0);
            lblLevel_4MarksValue.Margin = new Padding(6, 28, 0, 0);
            lblLevel_5Marks.Margin = new Padding(26, 28, 0, 0);
            lblLevel_5MarksValue.Margin = new Padding(6, 28, 0, 0);
            lblLevel_6Marks.Margin = new Padding(26, 28, 0, 0);
            lblLevel_6MarksValue.Margin = new Padding(6, 28, 0, 0);
            lblTotalAverage.Margin = new Padding(26, 28, 0, 0);
            lblTotalAverageValue.Margin = new Padding(6, 28, 0, 0);
            lblGrade.Margin = new Padding(26, 28, 0, 0);
            lblGradeValue.Margin = new Padding(6, 28, 0, 0);


            lblStudentName.Font = new Font(Font.FontFamily, 10, FontStyle.Bold);
            lblStudentNameValue.Font = new Font(Font.FontFamily, 10, FontStyle.Bold);
            lblCourseName.Font = new Font(Font.FontFamily, 10, FontStyle.Bold);
            lblCourseNameValue.Font = new Font(Font.FontFamily, 10, FontStyle.Bold);
            lblLevel_4Marks.Font = new Font(Font.FontFamily, 10, FontStyle.Bold);
            lblLevel_4MarksValue.Font = new Font(Font.FontFamily, 10, FontStyle.Bold);
            lblLevel_5Marks.Font = new Font(Font.FontFamily, 10, FontStyle.Bold);
            lblLevel_5MarksValue.Font = new Font(Font.FontFamily, 10, FontStyle.Bold);
            lblLevel_6Marks.Font = new Font(Font.FontFamily, 10, FontStyle.Bold);
            lblLevel_6MarksValue.Font = new Font(Font.FontFamily, 10, FontStyle.Bold);
            lblTotalAverage.Font = new Font(Font.FontFamily, 10, FontStyle.Bold);
            lblTotalAverageValue.Font = new Font(Font.FontFamily, 10, FontStyle.Bold);
            lblGrade.Font = new Font(Font.FontFamily, 10, FontStyle.Bold);
            lblGradeValue.Font = new Font(Font.FontFamily, 10, FontStyle.Bold);


            this.Controls.Add(lblStudentName, 0, 0);
            this.Controls.Add(lblStudentNameValue, 1, 0);
            this.Controls.Add(lblCourseName, 0, 1);
            this.Controls.Add(lblCourseNameValue, 1, 1);
            this.Controls.Add(lblLevel_4Marks, 0, 2);
            this.Controls.Add(lblLevel_4MarksValue, 1, 2);
            this.Controls.Add(lblLevel_5Marks, 0, 3);
            this.Controls.Add(lblLevel_5MarksValue, 1, 3);
            this.Controls.Add(lblLevel_6Marks,0, 4);
            this.Controls.Add(lblLevel_6MarksValue, 1, 4);
            this.Controls.Add(lblTotalAverage, 0, 5);
            this.Controls.Add(lblTotalAverageValue, 1, 5);
            this.Controls.Add(lblGrade, 0, 6);
            this.Controls.Add(lblGradeValue, 1, 6);

        }

        // Setting the component sizes
        public void setComponentSizes(System.Drawing.Size size)
        {
            size.Width = size.Width - 12;
            size.Height = size.Height - 50;
            this.Size = size;
  
            lblStudentName.Width = (this.Width / 2) - 60;
            lblStudentNameValue.Width = (this.Width / 2) - 60;
            lblCourseName.Width = (this.Width / 2) - 60;
            lblCourseNameValue.Width = (this.Width / 2) - 60;
            lblLevel_4Marks.Width = (this.Width / 2) - 60;
            lblLevel_4MarksValue.Width = (this.Width / 2) - 60;
            lblLevel_5Marks.Width = (this.Width / 2) - 60;
            lblLevel_5MarksValue.Width = (this.Width / 2) - 60;
            lblLevel_6Marks.Width = (this.Width / 2) - 60;
            lblLevel_6MarksValue.Width = (this.Width / 2) - 60;
            lblTotalAverage.Width = (this.Width / 2) - 60;
            lblTotalAverageValue.Width = (this.Width / 2) - 60;
            lblGrade.Width = (this.Width / 2) - 60;
            lblGradeValue.Width = (this.Width / 2) - 60;


            lblStudentName.Height = (this.Height / 15);
            lblStudentNameValue.Height = (this.Height / 15);
            lblCourseName.Height = (this.Height / 15);
            lblCourseNameValue.Height = (this.Height / 15);
            lblLevel_4Marks.Height = (this.Height / 15);
            lblLevel_4MarksValue.Height = (this.Height / 15);
            lblLevel_5Marks.Height = (this.Height / 15);
            lblLevel_5MarksValue.Height = (this.Height / 15);
            lblLevel_6Marks.Height = (this.Height / 15);
            lblLevel_6MarksValue.Height = (this.Height / 15);
            lblTotalAverage.Height = (this.Height / 15);
            lblTotalAverageValue.Height = (this.Height / 15);
            lblGrade.Height = (this.Height / 15);
            lblGradeValue.Height = (this.Height / 15);
        }
    }
}
