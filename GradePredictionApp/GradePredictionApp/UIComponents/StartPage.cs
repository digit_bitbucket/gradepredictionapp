﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;



namespace GradePredictionApp.UIComponents
{
    class StartPage : TabPage
    {
        private Button btnBuild;
        private MainScreenForm parentFormReference;

        public StartPage(MainScreenForm parentFormReference)
        {           
            this.Text = "START";
            this.BackColor = Color.SteelBlue;      

            this.parentFormReference = parentFormReference;

            addBuildButton();
 
        }

        // Adding the single mmain buttom to the initial screen
        private void addBuildButton()
        {
            btnBuild = new Button();
            btnBuild.Click += new EventHandler(btnBuild_Click);
            btnBuild.Text = "BUILD YOUR COURSE ";
            btnBuild.Font = new Font(Font.FontFamily, 10, FontStyle.Bold);
            btnBuild.Width = 300;
            btnBuild.Height = 40;

            setButtonLocation(this.Width / 2, this.Height / 2);
            this.Controls.Add(btnBuild);          
        }

        private void btnBuild_Click(object sender, EventArgs e)
        {
            // Display the custom class to get the input
            CustomInputDialog cut = new CustomInputDialog(this);
            cut.ShowDialog();
        }

        private void setButtonLocation(int left, int top)
        {
            btnBuild.Left = left - (btnBuild.Width / 2);
            btnBuild.Top = top - (btnBuild.Height / 2);
        }

        public void setComponentSizes(Size size)
        {
            this.Size = size;
            setButtonLocation(this.Width / 2, this.Height / 2);
        }

        // Display all tabs on the tab control
        public void displayAllPanelsOnTab()
        {
            parentFormReference.displayAllPanels();
        }
    }
}
