﻿using GradePredictionApp.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GradePredictionApp.UIComponents
{
    class SummaryTabPage : TabPage
    {

        private Panel summaryPanelHolder;
        private CourseSummaryPanel courseSummaryPanel;

        public SummaryTabPage(Size size)
        {
            this.Text = "SUMMARY";
            this.BackColor = Color.WhiteSmoke;
            this.Size = size;

            createUI();                 
        } 


        private void createUI()
        {
            this.Controls.Clear();
            summaryPanelHolder = new Panel();
            summaryPanelHolder.Size = this.Size;
            summaryPanelHolder.Height = this.Height - 40;
            this.Controls.Add(summaryPanelHolder);

            courseSummaryPanel = new CourseSummaryPanel(summaryPanelHolder.Size);
            summaryPanelHolder.Controls.Add(courseSummaryPanel);

            setComponentSizes(this.Size);
        }
        
        public void setComponentSizes(Size size)
        {
            summaryPanelHolder.Size = this.Size;
            courseSummaryPanel.setComponentSizes(summaryPanelHolder.Size);
        }

        // Refreshing to insert modified object values to the ui
        public void refreshPage()
        {
            createUI();
        }

    }
}
