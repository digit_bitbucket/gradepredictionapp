﻿using GradePredictionApp.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GradePredictionApp.UIComponents
{
    class LevelModuleDetailsPanel : TableLayoutPanel
    {

        private List<Module> moduleList;
        private List<Label> labelArray;

        public LevelModuleDetailsPanel(Size size, List<Module> moduleList)
        {
            this.Size = size;
            this.BackColor = Color.Lavender;
            this.moduleList = moduleList;
                      

            labelArray = new List<Label>();

            // Setting the padding to make the panel vertically scrollable
            Padding p = this.Padding;
            this.Padding = new Padding(p.Left, p.Top, SystemInformation.VerticalScrollBarWidth, p.Bottom);

            this.Dock = DockStyle.Right;
            this.AutoScroll = true;

            createUi();

        }

        private void createUi()
        {
            // Setting the simensions of the table layout
            this.ColumnCount = 3;
            if (moduleList != null)
            {
                this.RowCount = moduleList.Count + 1;
            }
            
            // Checking whether the level details are to be found
            if (moduleList == null)
            {
                MessageBox.Show("Level details not found!!! :( :(", "Data not found");
            }
            else
            {
                Label title_1 = new Label();
                title_1.Text = "NAME";
                title_1.Font = new Font(Font.FontFamily, 8, FontStyle.Bold);
                this.Controls.Add(title_1, 0, 0);

                Label title_2 = new Label();
                title_2.Text = "CREDITS";
                title_2.Font = new Font(Font.FontFamily, 8, FontStyle.Bold);
                this.Controls.Add(title_2, 1, 0);

                Label title_3 = new Label();
                title_3.Text = "AVERAGE";
                title_3.Font = new Font(Font.FontFamily, 8, FontStyle.Bold);
                this.Controls.Add(title_3, 2, 0);


                int i = 1;

                // Iteratig through the modules and inserting to the table layout
                foreach (var currentModule in moduleList)
                {
                    Label nameLabel = new Label();
                    nameLabel.Text = currentModule.getModuleName();

                    Label weightLabel = new Label();
                    weightLabel.Text = currentModule.getModuleCreditLevel() + " CREDITS";

                    Label marksLabel = new Label();
                    marksLabel.Text = currentModule.getModuleMark() + " %";

                    this.Controls.Add(nameLabel, 0, i);
                    this.Controls.Add(weightLabel, 1, i);
                    this.Controls.Add(marksLabel, 2, i);

                    labelArray.Add(nameLabel);
                    labelArray.Add(weightLabel);
                    labelArray.Add(marksLabel);

                    i++;
                }
            }
        }

        // Setting the sizes of components
        public void setComponentSizes(Size size)
        {
            this.Size = size;
            foreach (var currentLabel in labelArray)
            {
                currentLabel.Width = (this.Width / 3) - 30;
            }
        }
    }
}
