﻿using GradePredictionApp.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GradePredictionApp.UIComponents
{
    public class LevelTabPage : TabPage
    {

        private SplitContainer verticalSplitContainer;
        private SplitContainer horizontalSplitContainer;
        private SplitContainer horizontalSecondarySplitContainer;        
        private SplitterPanel leftPanel;
        private SplitterPanel rightPanel;
        private SplitterPanel upPanel;
        private SplitterPanel downPanel;
        private SplitterPanel upSecondaryPanel; 
        private SplitterPanel downSecondaryPanel;


        private YearComponentHolder topComponentRow;
        private YearModuleHolder leftModuleColumn;
        private YearBottomControls bottomControls;

        private ComponentMarksPanel currentMarkPanel;
        private ModuleSummaryPanel currentModulePanel;
        private LevelSummaryPanel currentLevelPanel;

        private Panel currentPanelHolder;
       

        public LevelTabPage(String pageTitle, Size size)
        {
            this.Text = pageTitle;            
            this.Size = size;
            createUI();
              
        }
        

        private void createUI()
        {
            // Verticle Split             
            verticalSplitContainer = new SplitContainer();
            verticalSplitContainer.Orientation = Orientation.Vertical;
            verticalSplitContainer.BackColor = Color.Blue;
            
            leftPanel = verticalSplitContainer.Panel1;
            leftPanel.BackColor = Color.Green;
            
            rightPanel = verticalSplitContainer.Panel2;
            rightPanel.BackColor = Color.OrangeRed;
            
            verticalSplitContainer.Panel1Collapsed = false;
            verticalSplitContainer.Panel2Collapsed = false;
            
            verticalSplitContainer.Size = this.Size;

            // Horizontal Primary Split          
            horizontalSplitContainer = new SplitContainer();
            horizontalSplitContainer.Orientation = Orientation.Horizontal;
            horizontalSplitContainer.BackColor = Color.Brown;
            
            upPanel = horizontalSplitContainer.Panel1;
            upPanel.BackColor = Color.AliceBlue;
            
            downPanel = horizontalSplitContainer.Panel2;
            downPanel.BackColor = Color.Khaki;
            
            horizontalSplitContainer.Panel1Collapsed = false;
            horizontalSplitContainer.Panel2Collapsed = false;
            
            horizontalSplitContainer.Size = verticalSplitContainer.Panel2.Size;
                
            //Horizontal Secondary Split            
            horizontalSecondarySplitContainer = new SplitContainer();
            horizontalSecondarySplitContainer.Orientation = Orientation.Horizontal;
            horizontalSecondarySplitContainer.BackColor = Color.Pink;
            
            upSecondaryPanel = horizontalSecondarySplitContainer.Panel1;
            upSecondaryPanel.BackColor = Color.Firebrick;
            downSecondaryPanel = horizontalSecondarySplitContainer.Panel2;
            downSecondaryPanel.BackColor = Color.Olive;
            
            horizontalSecondarySplitContainer.Panel1Collapsed = false;
            horizontalSecondarySplitContainer.Panel2Collapsed = false;
            horizontalSecondarySplitContainer.Size = horizontalSplitContainer.Panel2.Size;

            
            downPanel.Controls.Add(horizontalSecondarySplitContainer);   
            rightPanel.Controls.Add(horizontalSplitContainer);
            this.BackColor = Color.Black;            
            this.Controls.Add(verticalSplitContainer); 
          

            // Set to splitters to fixed borders
            verticalSplitContainer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            horizontalSplitContainer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            horizontalSecondarySplitContainer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            
            verticalSplitContainer.IsSplitterFixed = true;
            horizontalSplitContainer.IsSplitterFixed = true;
            horizontalSecondarySplitContainer.IsSplitterFixed = true;

            // Set sizes
            verticalSplitContainer.SplitterDistance = 115;
            horizontalSplitContainer.SplitterDistance = 75;
            horizontalSecondarySplitContainer.SplitterDistance = horizontalSecondarySplitContainer.Height - 75;
            setComponentSizes(this.Size);


            // Add Other panels
            topComponentRow = new YearComponentHolder(upPanel.Size, this);
            leftModuleColumn = new YearModuleHolder(leftPanel.Size, this);
            bottomControls = new YearBottomControls(downSecondaryPanel.Size, this);
            currentPanelHolder = new Panel();
            upPanel.Controls.Add(topComponentRow);
            leftPanel.Controls.Add(leftModuleColumn);
            downSecondaryPanel.Controls.Add(bottomControls);
            upSecondaryPanel.Controls.Add(currentPanelHolder);

            // Refresh the Ui to show data from the object in memory
            refresh();
        }


        // Adding module details (components) to the top bar
        public void addModuleDetailsToContainer(String moduleName)
        {
            Module module = BuisnessLogic.getModuleDetails(this.Text, moduleName.ToUpper());
            topComponentRow.addModuleControls(module);
        }
        

        // Adding a new module to the module holder 
        public void addModule(String moduleName)
        {
            leftModuleColumn.addModuleButton(moduleName);
        }

        // Removing a module from the memory and Ui
        public void deleteModule()
        {
            if ((currentModulePanel != null)&&(currentPanelHolder.Controls.Contains(currentModulePanel))){
                BuisnessLogic.removeModule(this.Text, currentModulePanel.getSelectedModule());
                refresh();
                topComponentRow.Controls.Clear();
            }
            else
            {
                MessageBox.Show("Deletion can only ne done after navigating to the relevant module summary!!!","Invalid Operation");
            }
        }

        public String getTabTitle()
        {
            return this.Text;
        }

        // Display the module summary in the middle panel
        public void displayModuleSummary(String moduleName)
        {
            currentPanelHolder.Controls.Clear();
            currentModulePanel = new ModuleSummaryPanel(upSecondaryPanel.Size, BuisnessLogic.getModuleDetails(this.Text, moduleName));
            currentPanelHolder.Controls.Add(currentModulePanel);
            currentPanelHolder.Width = upSecondaryPanel.Width - 20;
            currentPanelHolder.Height = upSecondaryPanel.Height;
            currentModulePanel.setComponentSizes(currentPanelHolder.Size); 
        }

        // Display the level summary in the middle panel
        public void displayLevelSummary()
        {
            currentPanelHolder.Controls.Clear();
            currentLevelPanel = new LevelSummaryPanel(upSecondaryPanel.Size, BuisnessLogic.getLevel(this.Text));
            currentPanelHolder.Controls.Add(currentLevelPanel);
            currentPanelHolder.Width = upSecondaryPanel.Width - 20;
            currentPanelHolder.Height = upSecondaryPanel.Height;
            currentLevelPanel.setComponentSizes(currentPanelHolder.Size); 
        }

        // Display the component details in the middle panel
        public void setComponentDetailsPanel(String selectedModuleName, String selectedComponentName)
        {
            currentPanelHolder.Controls.Clear();
            currentMarkPanel = new ComponentMarksPanel(upSecondaryPanel.Size, BuisnessLogic.getComponentDetails(this.Text, selectedModuleName, selectedComponentName), BuisnessLogic.getModuleDetails(this.Text, selectedModuleName));
            currentPanelHolder.Controls.Add(currentMarkPanel);
            currentPanelHolder.Width = upSecondaryPanel.Width - 20;
            currentPanelHolder.Height = upSecondaryPanel.Height;
            currentMarkPanel.setComponentSizes(currentPanelHolder.Size);
        }

        // Refresh the ui to show modified object data from memory
        public void refresh()
        {
            currentPanelHolder.Controls.Clear();
            List<String> nameList = BuisnessLogic.getModuleNameList(this.Text);
            if (nameList != null) {
                leftModuleColumn.createUi();
                foreach (var name in nameList)
                {
                    leftModuleColumn.addModuleButton(name);
                }
            }     
        }

        // Setting the ui element sizes
        public void setComponentSizes(Size size)
        {
            try
            {
                this.Size = size;
                verticalSplitContainer.Size = size;
                horizontalSplitContainer.Size = verticalSplitContainer.Panel2.Size;
                horizontalSecondarySplitContainer.Size = horizontalSplitContainer.Panel2.Size;
                topComponentRow.setComponentButtonSizes(upPanel.Size);
                leftModuleColumn.setModuleButtonSizes(leftPanel.Size);
                bottomControls.setComponentSizes(downSecondaryPanel.Size);

                currentPanelHolder.Width = upSecondaryPanel.Width - 20;
                currentPanelHolder.Height = upSecondaryPanel.Height;
                if (currentMarkPanel != null)
                {
                    currentMarkPanel.setComponentSizes(currentPanelHolder.Size);
                }
                if (currentModulePanel != null)
                {
                    currentModulePanel.setComponentSizes(currentPanelHolder.Size);
                }
            } catch (Exception ex){

            }

        }

    }
}
