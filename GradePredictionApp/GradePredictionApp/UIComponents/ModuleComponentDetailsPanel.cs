﻿using GradePredictionApp.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GradePredictionApp.UIComponents
{
    class ModuleComponentDetailsPanel : TableLayoutPanel
    {

        // Lists holding the components and Labels
        private List<Component> componentList;
        private List<Label> labelArray;

        public ModuleComponentDetailsPanel(Size size, List<Component> componentList)
        {
            this.Size = size;
            this.BackColor = Color.Lavender;
            this.componentList = componentList;
                      

            labelArray = new List<Label>();

            // Adding padding to make the panel vertically scrollable
            Padding p = this.Padding;
            this.Padding = new Padding(p.Left, p.Top, SystemInformation.VerticalScrollBarWidth, p.Bottom);
            this.Dock = DockStyle.Right;
            this.AutoScroll = true;

            createUi();
        }

        private void createUi()
        {
            // Defining the rows and columns of the table layout
            this.RowCount = 10;
            this.ColumnCount = 3;

            if (componentList == null)
            {
                // if component list is empty
                MessageBox.Show("Error :( :(");
            }
            else
            {

                Label title_1 = new Label();
                title_1.Text = "NAME";
                title_1.Font = new Font(Font.FontFamily, 8, FontStyle.Bold);
                this.Controls.Add(title_1, 0, 0);

                Label title_2 = new Label();
                title_2.Text = "WEIGHT";
                title_2.Font = new Font(Font.FontFamily, 8, FontStyle.Bold);
                this.Controls.Add(title_2, 1, 0);

                Label title_3 = new Label();
                title_3.Text = "MARKS";
                title_3.Font = new Font(Font.FontFamily, 8, FontStyle.Bold);
                this.Controls.Add(title_3, 2, 0);
                
                
                int i = 1;

                // setting the components in the list to the table's cells
                foreach (var currentComponent in componentList)
                {
                    Label nameLabel = new Label();
                    nameLabel.Text = currentComponent.getComponentName();

                    Label weightLabel = new Label();
                    weightLabel.Text = currentComponent.getComponentWeight() + " %";

                    Label marksLabel = new Label();
                    marksLabel.Text = currentComponent.getComponentMark() + " %";

                    this.Controls.Add(nameLabel, 0, i);
                    this.Controls.Add(weightLabel, 1, i);
                    this.Controls.Add(marksLabel, 2, i);

                    labelArray.Add(nameLabel);
                    labelArray.Add(weightLabel);
                    labelArray.Add(marksLabel);

                    i++;
                } 
            }

        }


        // Setting the element sizes
        public void setComponentSizes(Size size){
            this.Size = size;
            foreach (var currentLabel in labelArray){
                currentLabel.Width = (this.Width / 3) - 30;
            }
        }




    }
}
