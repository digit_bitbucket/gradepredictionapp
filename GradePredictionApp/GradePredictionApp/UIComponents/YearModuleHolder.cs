﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GradePredictionApp.UIComponents
{
    class YearModuleHolder : FlowLayoutPanel
    {
        
        private List<Button> buttonArray;
        private int numberOfButtons;
        private Button levelSummaryButton;
        private LevelTabPage parentPage;


        public YearModuleHolder(Size size, LevelTabPage parentPage)
        {
            this.BackColor = Color.Green;
            this.Size = size;
            this.parentPage = parentPage;

            // Hold every button for resizing later
            buttonArray = new List<Button>();
            numberOfButtons = 0;

            // Making the panel scrollable (verticaly)
            this.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;            
            this.AutoScroll = true;
            this.WrapContents = false;

            createUi();

        }



        public void createUi()
        {
            clearPanel();
            setSpecialButton();
        }

        public void clearPanel()
        {
            this.Controls.Clear();
        }
               
        // Insert the level summary button
        private void setSpecialButton()
        {
            levelSummaryButton = new Button();
            levelSummaryButton.Text = "LEVEL SUMMARY";
            buttonArray.Add(levelSummaryButton);
            this.Controls.Add(levelSummaryButton);
            if (this.Width > 20)
            {
                levelSummaryButton.Width = this.Width - 23;
            }
            levelSummaryButton.Height = 60;
            levelSummaryButton.Click += delegate
            {
                // Displays summary on the middle screen
                parentPage.displayLevelSummary();
            };     
        }

        //Adding a dynamic button
        public void addModuleButton(String moduleName)
        {
            Button newButton = new Button();
            buttonArray.Add(newButton);
            newButton.Text = moduleName;       
            this.Controls.Add(newButton);
            if (this.Width > 23)
            {
                newButton.Width = this.Width - 20;
            }
            newButton.Height = 60;
            if (!moduleName.Equals("LEVEL SUMMARY"))
            {
                newButton.Click += delegate
                {
                    // Adding module's component details to the top row
                    parentPage.addModuleDetailsToContainer(moduleName);
                };    
            }
        }

        
        public void setModuleButtonSizes(Size size)
        {
            this.Width = size.Width;
            this.Height = size.Height - 60;

            // Iterate the button to change the sizes
            foreach (var currentButton in buttonArray)
            {
                if ((currentButton != null) && (this.Width > 23))
                {
                    currentButton.Width = this.Width - 23;
                }
            }

            if ((levelSummaryButton != null) && (this.Width > 23))
            {
                levelSummaryButton.Width = this.Width - 23;
            }
        }
       
    }
}