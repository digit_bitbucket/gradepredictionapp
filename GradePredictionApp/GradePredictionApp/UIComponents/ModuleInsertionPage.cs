﻿using GradePredictionApp.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GradePredictionApp.UIComponents
{
    class ModuleInsertionPage : TableLayoutPanel
    {
        
        private Panel componentTablePanelHolder;
        private ComponentWeightPanel componentTablePanel;
        private LevelTabPage parentTabPage;
        private AddModuleScreen parentScreen;

        private Label lblTitle;
        private Label lblModuleCode;
        private Label lblModuleName;
        private Label lblCreditLevel;
        private Label lblNoOfComponent;

        private TextBox txtModuleCode;
        private TextBox txtModuleName;

        private ComboBox comboCreditLevel;
        private ComboBox comboNoOfComponents;

        private Button btnSave;
        private Button btnCancel;

   
        public ModuleInsertionPage(Size size, LevelTabPage parentTabPage, AddModuleScreen parentScreen)
        {
            this.BackColor = Color.Orange;
            this.Size = size;   
            this.ColumnCount = 2;
            this.RowCount = 10;
            this.parentTabPage = parentTabPage;
            this.parentScreen = parentScreen;

            lblTitle = new Label();
            lblModuleCode = new Label();
            lblModuleName = new Label();
            lblCreditLevel = new Label();
            lblNoOfComponent = new Label();

            txtModuleCode = new TextBox();
            txtModuleName = new TextBox();            

            // Test
                txtModuleCode.Text = "C001";
                txtModuleName.Text = "Module 01";


            comboCreditLevel = new ComboBox();
            comboNoOfComponents = new ComboBox();

            componentTablePanelHolder = new Panel();

            btnSave = new Button();

            btnCancel = new Button();

            createUi();            
        }

        private void createUi()
        {

            componentTablePanelHolder.BackColor = Color.Green;


            lblTitle.Text = "MODULE INSERTION"; 
            lblTitle.Font = new Font(Font.FontFamily, 18, FontStyle.Bold);
            lblModuleCode.Text = "* MODULE CODE :";
            lblModuleName.Text = "* MODULE NAME :";
            lblCreditLevel.Text = "* CREDIT LEVEL : ";
            lblNoOfComponent.Text = "* NO. OF COMPONENTS :";
            lblModuleCode.Margin = new Padding(6, 6, 0, 0);
            lblModuleName.Margin = new Padding(6, 6, 0, 0);
            lblCreditLevel.Margin = new Padding(6, 6, 0, 0);
            lblNoOfComponent.Margin = new Padding(6, 6, 0, 0);

            comboCreditLevel.Items.Add("15 CREDITS");
            comboCreditLevel.Items.Add("30 CREDITS");
            comboCreditLevel.DropDownStyle = ComboBoxStyle.DropDownList;
            comboCreditLevel.SelectedIndex = 0;

            comboNoOfComponents.Items.Add("ONE");
            comboNoOfComponents.Items.Add("TWO");
            comboNoOfComponents.Items.Add("THREE");
            comboNoOfComponents.Items.Add("FOUR");
            comboNoOfComponents.Items.Add("FIVE");
            comboNoOfComponents.DropDownStyle = ComboBoxStyle.DropDownList;
            comboNoOfComponents.SelectedIndex = 0;

            btnSave.Text = "SAVE";
            btnCancel.Text = "CANCEL";

            this.Controls.Add(lblTitle, 0, 0);
            this.SetColumnSpan(lblTitle, 2);
            this.Controls.Add(lblModuleCode, 0, 1);
            this.Controls.Add(lblModuleName, 0, 2);
            this.Controls.Add(lblCreditLevel, 0, 3);
            this.Controls.Add(lblNoOfComponent, 0, 4);
            this.Controls.Add(componentTablePanelHolder, 0, 5);
            this.SetColumnSpan(componentTablePanelHolder, 2);
            this.Controls.Add(txtModuleCode, 1, 1);
            this.Controls.Add(txtModuleName, 1, 2);
            this.Controls.Add(comboCreditLevel, 1, 3);
            this.Controls.Add(comboNoOfComponents, 1, 4);
            this.Controls.Add(btnSave, 0, 6);
            this.Controls.Add(btnCancel, 1, 6);

            setComponentSizes(this.Size);
            
            btnSave.Click += new EventHandler(btnSave_Click);
            btnCancel.Click += new EventHandler(btnCancel_Click);
            comboNoOfComponents.SelectedIndexChanged += new EventHandler(comboNoOfComponents_change);
            comboNoOfComponents.SelectedIndex = 1;
            comboNoOfComponents.SelectedIndex = 0;
        }

        private void comboNoOfComponents_change(object sender, EventArgs e)
        {
            int numberOfComponents = 0;
            if (comboNoOfComponents.SelectedIndex == 0)
            {
                numberOfComponents = 1;
            }
            else if (comboNoOfComponents.SelectedIndex == 1)
            {
                numberOfComponents = 2;
            }
            else if (comboNoOfComponents.SelectedIndex == 2)
            {
                numberOfComponents = 3;
            }
            else if (comboNoOfComponents.SelectedIndex == 3)
            {
                numberOfComponents = 4;
            }
            else if (comboNoOfComponents.SelectedIndex == 4)
            {
                numberOfComponents = 5;
            }

            componentTablePanelHolder.Controls.Clear();
            componentTablePanel = new ComponentWeightPanel(this.Size, numberOfComponents);
            componentTablePanelHolder.Controls.Add(componentTablePanel);
        }
              

     
        private void btnSave_Click(object sender, EventArgs e)
        {           
            Boolean isValuesValid = true;     
            TrackBar[] trackBarArray = componentTablePanel.getTrackBarArray();
            int totalWeight = 0;

            for (int i = 0; i < componentTablePanel.getNumberOfComponents() ; i++)
            {
                if (componentTablePanel.getNumberOfComponents() > 1)
                {
                    totalWeight = totalWeight + trackBarArray[i].Value;
                    if ((trackBarArray[i].Value == 0) | (trackBarArray[i].Value == 100))
                    {
                        isValuesValid = false;
                        break;
                    }
                }
            }

            if (componentTablePanel.getNumberOfComponents() == 1)
            {
                totalWeight = 100;
            }

            if (checkModuleIdInLevel())
            {
                MessageBox.Show("Please enter a different module ID!!!", "Invalid Module ID");  
            }
            else if (checkModuleNameInLevel())
            {
                MessageBox.Show("Please enter a different module name!!!", "Invalid Module Name");            
            }
            else
            {
                if (isValuesValid)
                {
                    if ((!txtModuleCode.Text.ToString().Equals("")) && (!txtModuleName.Text.ToString().Equals("")))
                    {
                        if (totalWeight == 100)
                        {                 
                            // logic
                            List<Component> componentList = new List<Component>();
                            for (int i = 0; i < componentTablePanel.getNumberOfComponents(); i++)
                            {
                                Component newComponent = new Component(componentTablePanel.getComponentNameFromArray(i).ToString(), componentTablePanel.getComponentWeightFromArray(i));
                                componentList.Add(newComponent);
                            }

                            int creditLevel;
                            if (comboCreditLevel.SelectedIndex == 0)
                            {
                                creditLevel = 15;
                            }
                            else
                            {
                                creditLevel = 30;
                            }

                            BuisnessLogic.addModule(parentTabPage.Text.ToString(), txtModuleCode.Text.ToString(), txtModuleName.Text.ToString(), creditLevel, componentList);

                            parentTabPage.addModule(txtModuleName.Text.ToString().ToUpper());
                            parentScreen.Close();
                        }
                        else
                        {
                            MessageBox.Show("Total component weight should be 100!!!", "Invalid Weight");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Please enter all fields!!!", "Incomplete Fields");
                    }
                }
                else
                {
                    MessageBox.Show("Please select values between 1 and 99 as the weight!!!", "Invalid Weight");
                }
            }    
        }


        private void btnCancel_Click(object sender, EventArgs e)
        {
            parentScreen.Dispose();
        }

        private Boolean checkModuleNameInLevel()
        {
            return BuisnessLogic.checkIfModuleNameIsInLevel(txtModuleName.Text.ToString());
        }

        private Boolean checkModuleIdInLevel()
        {
            return BuisnessLogic.checkIfModuleIdIsInLevel(txtModuleCode.Text.ToString());
        }

        public void setComponentSizes(Size size)
        {
            try
            {
                this.Size = size;

                lblTitle.Width = this.Width;
                lblTitle.TextAlign = ContentAlignment.MiddleCenter;
                lblModuleCode.Width = this.Width / 2;
                lblModuleName.Width = this.Width / 2;
                lblCreditLevel.Width = this.Width / 2;
                lblNoOfComponent.Width = this.Width / 2;
                componentTablePanelHolder.Width = this.Width - 23;
                txtModuleName.Width = (this.Width / 2) - 30;
                txtModuleCode.Width = (this.Width / 2) - 30;
                comboNoOfComponents.Width = (this.Width / 2) - 30;
                comboCreditLevel.Width = (this.Width / 2) - 30;
                btnCancel.Width = (this.Width / 2) - 26;
                btnSave.Width = (this.Width / 2) - 26;

                lblTitle.Height = (this.Height / 10) * 2;
                lblModuleCode.Height = (this.Height / 10) * 1;
                lblModuleName.Height = (this.Height / 10) * 1;
                lblCreditLevel.Height = (this.Height / 10) * 1;
                lblNoOfComponent.Height = (this.Height / 10) * 1;
                componentTablePanelHolder.Height = (int)((this.Height / 10) * 1.5);
                txtModuleCode.Height = (this.Height / 10) * 1;
                txtModuleName.Height = (this.Height / 10) * 1;
                comboCreditLevel.Height = (this.Height / 10) * 1;
                comboNoOfComponents.Height = (this.Height / 10) * 1;
                btnCancel.Height = ((this.Height / 10) * 1) - 10;
                btnSave.Height = ((this.Height / 10) * 1) - 10;

                componentTablePanel.setComponentSizes(new Size(this.Width, (int)((this.Height / 10) * 1.5)));
            }
            catch (Exception ex)
            {
            }
        }


    }
}
