﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GradePredictionApp.UIComponents
{

    class CustomInputDialog : Form
    {
       
        private Label studentName = new Label();
        private Label courseName = new Label();

        private TextBox txtStudentName;
        private TextBox txtCourseName;

        private Button btnOkay;
        private Button btnCancel;

        private StartPage parentPageReference;

        public CustomInputDialog(StartPage parentPageReference)
        {
            this.Size = new Size(400, 180);
            this.parentPageReference = parentPageReference;

            // Restricting the maximizing control
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;

            this.Text = "GET STARTED >>>";
            this.BackColor = Color.YellowGreen;

            studentName = new Label();
            courseName = new Label();

            txtStudentName = new TextBox();
            txtCourseName = new TextBox();
            
            btnOkay = new Button();
            btnCancel = new Button();

            CreateUi();
        }


        private void CreateUi()
        {
            studentName.Text = "STUDENT NAME :";
            courseName.Text = "COURSE NAME :";

            btnOkay.Text = "PROCEED";
            btnCancel.Text = "CANCEL";
            txtCourseName.Text = "MY COURSE";
            txtStudentName.Text = "MY NAME";

            // Checking if the name field is empty
            txtStudentName.LostFocus += delegate
            {
                if (txtStudentName.Text.ToString().Equals(""))
                {
                    MessageBox.Show("Student name cannot be empty!!!", "Invalid input");
                    txtStudentName.Focus();
                }
            };

            // Checking if the course name is empty
            txtCourseName.LostFocus += delegate
            {

                if (txtCourseName.Text.ToString().Equals(""))
                {
                    MessageBox.Show("Course name cannot be empty!!!", "Invalid input");
                    txtCourseName.Focus();
                }
            };


            btnCancel.Click += delegate
            {
                this.Dispose();
            };


            btnOkay.Click += delegate
            {
                // Creating a new course and displaying all the panels
                BuisnessLogic.buildCourse(txtStudentName.Text, txtCourseName.Text);
                parentPageReference.displayAllPanelsOnTab();
                this.Dispose();
            };


            studentName.SetBounds(20, 20, 100, 20);
            this.Controls.Add(studentName);
            courseName.SetBounds(20, 60, 100, 20);
            this.Controls.Add(courseName);
            btnOkay.SetBounds(20, 100, 150, 30);
            this.Controls.Add(btnOkay);
            btnCancel.SetBounds(200, 100, 150, 30);
            this.Controls.Add(btnCancel);
            txtStudentName.SetBounds(120, 18, 230, 30);
            this.Controls.Add(txtStudentName);
            txtCourseName.SetBounds(120, 58, 230, 30);
            this.Controls.Add(txtCourseName);
        }
    }
}
