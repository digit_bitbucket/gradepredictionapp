﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GradePredictionApp.UIComponents
{
    class ComponentWeightPanel : TableLayoutPanel
    {

        private int numberOfComponents;
        private Label[] percentageLabelArray;
        private TextBox[] nameTextArray;
        private TrackBar[] trackBarArray;

        public ComponentWeightPanel(Size size, int numberOfComponents)
        {

            this.Size = size;
            this.BackColor = Color.OrangeRed; 
            this.numberOfComponents = numberOfComponents;

            // Setting the padding to make the panel vertically scrollable
            Padding p = this.Padding;
            this.Padding = new Padding( p.Left, p.Top, SystemInformation.VerticalScrollBarWidth, p.Bottom);

            createUi(numberOfComponents);

            // More scrollling related properties
            this.Dock = DockStyle.Right;       
            this.AutoScroll = true;           
        }

        private void createUi(int numberOfComponents)
        {
            this.ColumnCount = 4;
            this.RowCount = numberOfComponents;


            // Arrays which hold the labels, text boxes and trackbars
            percentageLabelArray = new Label[numberOfComponents];
            nameTextArray = new TextBox[numberOfComponents];
            trackBarArray = new TrackBar[numberOfComponents];

            // Iterarting the arrays to insert the items to the table layout
            for (int i = 0; i < numberOfComponents; i++)
            {
                // Loosing space with a transparent label;
                Label freeSpace = new Label();
                freeSpace.Width = 10;
                freeSpace.Margin = new Padding(SystemInformation.VerticalScrollBarWidth, 0, 0, 0);

                TextBox txtComponentName = new TextBox();
                txtComponentName.Text = "COMPONENT 0" + (i + 1) + "";
                txtComponentName.Margin = new Padding(6, 8, 0, 0);

                txtComponentName.Leave += delegate
                {
                    if (txtComponentName.Text.ToString().Equals(""))
                    {
                        // If the component name is left blank
                        MessageBox.Show("Please enter a valid component name!!!","Invalid component name");
                        txtComponentName.Focus();
                    }
                };

                Label lblComponentPercentage = new Label();
                lblComponentPercentage.Text = "0%";
                lblComponentPercentage.Margin = new Padding(6, 8, 0, 0);

                // Setting the trackbar controls
                TrackBar percentageTrackBar = new TrackBar();
                percentageTrackBar.Maximum = 100;                
                percentageTrackBar.Minimum = 0;
                percentageTrackBar.TickFrequency = 10;
                percentageTrackBar.Width = this.Width - 300;        

                // Actions when percentage trackbar is changed
                percentageTrackBar.ValueChanged += delegate
                {
                    lblComponentPercentage.Text = percentageTrackBar.Value.ToString() + "%";
                    if (numberOfComponents == 2){

                        // If two components, one components weight is the remaining weight from 100
                        if (percentageTrackBar == trackBarArray[0])
                        {
                            trackBarArray[1].Value = 100 - percentageTrackBar.Value;                            
                        }
                        else
                        {
                            trackBarArray[0].Value = 100 - percentageTrackBar.Value;       
                        }
                    }
                };     
                
                this.Controls.Add(freeSpace, 0, i);
                this.Controls.Add(txtComponentName, 1, i);
                this.Controls.Add(percentageTrackBar, 2, i);
                this.Controls.Add(lblComponentPercentage, 3, i);

                percentageLabelArray[i] = lblComponentPercentage;
                nameTextArray[i] = txtComponentName;
                trackBarArray[i] = percentageTrackBar;
            }

            if (numberOfComponents == 1)
            {
                trackBarArray[0].Value = 100;
                trackBarArray[0].Enabled = false;
            }

            setComponentSizes(this.Size);
        }

        // Setting up element sizes
        public void setComponentSizes(Size size)
        {
            this.Size = size;
            for (int i = 0; i < numberOfComponents; i++)
            {
                if (nameTextArray[i] != null)
                {
                    nameTextArray[i].Width = (int)((size.Width / 10) * 2.5) - 10;                
                }
            }
            for (int i = 0; i < numberOfComponents; i++)
            {
                if (percentageLabelArray[i] != null)
                {
                    percentageLabelArray[i].Width = (size.Width / 10) - 10;
                }
            }
            for (int i = 0; i < numberOfComponents; i++)
            {
                if (trackBarArray[i] != null)
                {
                    trackBarArray[i].Width = (int) (((size.Width / 10) * 5.5) - 10);
                }
            }
        }
                       

        public TrackBar[] getTrackBarArray()
        {
            return trackBarArray;
        }

        public int getNumberOfComponents()
        {
            return numberOfComponents;
        }

        public String getComponentNameFromArray(int index)
        {
            return nameTextArray[index].Text.ToString();
        }

        public int getComponentWeightFromArray(int index)
        {
            return trackBarArray[index].Value;
        }
    }
}
