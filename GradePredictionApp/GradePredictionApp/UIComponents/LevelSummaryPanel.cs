﻿using GradePredictionApp.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GradePredictionApp.UIComponents
{
    class LevelSummaryPanel : TableLayoutPanel
    {
        
        private Label lblLevel;
        private Label lblLevelValue;
        private Label lblModuleCredits;
        private Label lblModuleCreditsValue;
        private Label lblLevelMarks;
        private Label lblLevelMarksValue;

        private Panel levelSummaryPanelHolder;
        private LevelModuleDetailsPanel moduleDetailsPanel;

        private Level selectedLevel;


        public LevelSummaryPanel(Size size, Level selectedLevel)
        {
            this.BackColor = Color.DarkGoldenrod;
            this.Size = size;
            this.selectedLevel = selectedLevel;

            createUi();

            // Defining the rows and column numbers of the table layout
            this.ColumnCount = 2;
            this.RowCount = 4;
        }


        private void createUi()
        {

            lblLevel = new Label();
            lblLevelValue = new Label();
            lblModuleCredits = new Label();
            lblModuleCreditsValue = new Label();
            lblLevelMarks = new Label();
            lblLevelMarksValue = new Label();


            lblLevel.Text = "LEVEL :";
            lblLevelValue.Text = selectedLevel.getLevelNumber() + "";
            lblModuleCredits.Text = "MODULE CREDITS :";
            lblModuleCreditsValue.Text = selectedLevel.getTotalModuleCredits() + "";
            lblLevelMarks.Text = "MARKS :";
            lblLevelMarksValue.Text = selectedLevel.getLevelMark() + " %";

            // Setting the margins
            lblLevel.Margin = new Padding(6, 8, 0, 0);
            lblLevelValue.Margin = new Padding(6, 8, 0, 0);
            lblModuleCredits.Margin = new Padding(6, 8, 0, 0);
            lblModuleCreditsValue.Margin = new Padding(6, 8, 0, 0);
            lblLevelMarks.Margin = new Padding(6, 8, 0, 0);
            lblLevelMarksValue.Margin = new Padding(6, 8, 0, 0);

            // Creating a panel to hold the level summary panel
            levelSummaryPanelHolder = new Panel();
            levelSummaryPanelHolder.BackColor = Color.Lavender;

            moduleDetailsPanel = new LevelModuleDetailsPanel(levelSummaryPanelHolder.Size, selectedLevel.getModuleArray());

            this.Controls.Add(lblLevel, 0, 0);
            this.Controls.Add(lblLevelValue, 1, 0);
            this.Controls.Add(lblModuleCredits, 0, 1);
            this.Controls.Add(lblModuleCreditsValue, 1, 1);
            this.Controls.Add(lblLevelMarks, 0, 2);
            this.Controls.Add(lblLevelMarksValue, 1, 2);
            this.Controls.Add(levelSummaryPanelHolder, 0, 3);
            this.SetColumnSpan(levelSummaryPanelHolder, 2);

            levelSummaryPanelHolder.Controls.Clear();
            levelSummaryPanelHolder.Controls.Add(moduleDetailsPanel);

            setComponentSizes(this.Size);
        }


        // Setting component sizes
        public void setComponentSizes(Size size)
        {
            this.Size = size;

            lblLevel.Width = (this.Width / 2) - 15;
            lblLevelValue.Width = (this.Width / 2) - 15;
            lblModuleCredits.Width = (this.Width / 2) - 15;
            lblModuleCreditsValue.Width = (this.Width / 2) - 15;
            lblLevelMarks.Width = (this.Width / 2) - 15;
            lblLevelMarksValue.Width = (this.Width / 2) - 15;

            levelSummaryPanelHolder.Width = this.Width - 10;

            lblLevel.Height = (this.Height / 7);
            lblLevelValue.Height = (this.Height / 7);
            lblModuleCredits.Height = (this.Height / 7);
            lblModuleCreditsValue.Height = (this.Height / 7);
            lblLevelMarks.Height = (this.Height / 7);
            lblLevelMarksValue.Height = (this.Height / 7);
    
            levelSummaryPanelHolder.Height = ((this.Height / 7) * 4) - 40;

            if (moduleDetailsPanel != null)
            {
                moduleDetailsPanel.setComponentSizes(levelSummaryPanelHolder.Size);
            } 
                
        }


    }
}
