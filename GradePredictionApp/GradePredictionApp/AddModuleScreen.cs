﻿using GradePredictionApp.UIComponents;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GradePredictionApp
{
    public partial class AddModuleScreen : Form
    {

        private ModuleInsertionPage insertionScreen;
        private LevelTabPage parentTabPage;

        public AddModuleScreen(LevelTabPage parentTabPage)
        {
            InitializeComponent();
            this.parentTabPage = parentTabPage;
        }

        private void AddModuleScreen_Load(object sender, EventArgs e)
        {
            createUiComponents();
        }

        private void createUiComponents()
        {
            insertionScreen = new ModuleInsertionPage(this.Size, parentTabPage, this);
            this.Controls.Add(insertionScreen);
        }

        private void AddModuleScreen_Resize(object sender, EventArgs e)
        {
            try
            {
                if (insertionScreen != null)
                {
                    insertionScreen.setComponentSizes(this.Size);
                }
            }
            catch (Exception ex)
            {

            }
        }

        public void disposeForm()
        {
            this.Dispose();
        }
        
    }
}
