﻿using GradePredictionApp.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GradePredictionApp
{
    class BuisnessLogic
    {

        // Checking whether the module id already exists
        public static Boolean checkIfModuleIdIsInLevel(String moduleId)
        {
            Boolean existsInLevel_4 = GlobalVariableHolder.course.getLevel_4().checkIfModuleIdExistsInLevel(moduleId);
            if (existsInLevel_4)
            {
                return true;
            }
            
            Boolean existsInLevel_5 = GlobalVariableHolder.course.getLevel_5().checkIfModuleIdExistsInLevel(moduleId);
            if (existsInLevel_5)
            {
                return true;
            }
            
            Boolean existsInLevel_6 = GlobalVariableHolder.course.getLevel_6().checkIfModuleIdExistsInLevel(moduleId);
            if (existsInLevel_6)
            {
                return true;
            }

            return false;
        }

        // Getting the level object from the  level name
        public static Level getLevel(String levelName)
        {
            if (levelName.Equals("LEVEL 04"))
            {
                return GlobalVariableHolder.course.getLevel_4();
            }
            else if (levelName.Equals("LEVEL 05"))
            {
                return GlobalVariableHolder.course.getLevel_5();
            }
            else if (levelName.Equals("LEVEL 06"))
            {
                return GlobalVariableHolder.course.getLevel_6();
            }
            return null;
        }

        // Checking if  the module name is existing in the level
        public static Boolean checkIfModuleNameIsInLevel(String moduleName)
        {
            Boolean existsInLevel_4 = GlobalVariableHolder.course.getLevel_4().checkIfModuleNameExistsInLevel(moduleName.ToUpper());
            if (existsInLevel_4)
            {
                return true;
            }

            Boolean existsInLevel_5 = GlobalVariableHolder.course.getLevel_5().checkIfModuleNameExistsInLevel(moduleName.ToUpper());
            if (existsInLevel_5)
            {
                return true;
            }

            Boolean existsInLevel_6 = GlobalVariableHolder.course.getLevel_6().checkIfModuleNameExistsInLevel(moduleName.ToUpper());
            if (existsInLevel_6)
            {
                return true;
            }  
            return false;
        }


        // Adding a new module to the relevant level
        public static void addModule(String level, String moduleID, String moduleName, int moduleCreditLevel, List<Component> componentList)
        {
            Module newModule = new Module(moduleID, moduleName.ToUpper(), moduleCreditLevel, componentList);
            if (level.Equals("LEVEL 04"))
            {
                GlobalVariableHolder.course.getLevel_4().AddModule(newModule);
            }
            else if (level.Equals("LEVEL 05"))
            {
                GlobalVariableHolder.course.getLevel_5().AddModule(newModule);
            }
            else if (level.Equals("LEVEL 06"))
            {
                GlobalVariableHolder.course.getLevel_6().AddModule(newModule);
            } 
        }

        // Getting  the module object from the module name and level name
        public static Module getModuleDetails(String level, String moduleName)
        {   
            List<Module> moduleList;
            if (level.Equals("LEVEL 04"))
            {
                moduleList = GlobalVariableHolder.course.getLevel_4().getModuleArray();   
            }
            else if (level.Equals("LEVEL 05"))
            {
                moduleList = GlobalVariableHolder.course.getLevel_5().getModuleArray();
            }
            else 
            {
                moduleList = GlobalVariableHolder.course.getLevel_6().getModuleArray(); 
            }
            foreach (var currentModule in moduleList)
            {
                if (currentModule.getModuleName().ToString().Equals(moduleName)){
                    return currentModule;
                }

            }
           return null;  
        }

        // Getting a component object from it's name
        public static Component getComponentDetails(String level, String moduleName, String componentName)
        {
            Module selectedModule = getModuleDetails(level, moduleName);
            if (selectedModule != null)
            {        
                return selectedModule.getComponentFromName(componentName);
            }
            return null;
        }

        // Setting a new component mark
        public static void setComponentMark(Component selectedComponent, int componentMark, Module selectedModule)
        {
            selectedComponent.setComponentMark(componentMark);
            List<Component> componentList = selectedModule.getComponentArray();
            int moduleMark = 0;

            // Calculating the new module mark
            foreach (var currentComponent in componentList){
                moduleMark = moduleMark + (currentComponent.getComponentMark() * currentComponent.getComponentWeight() / 100);
            }
            
            //Setting the modified module mark
            selectedModule.setModuleMark(moduleMark);

            // Calculating the modified level mark
            calculateLevelMark();

            // Calculating the modified average course mark
            calculateCourseAverage();
        }

        // Calculating the level mark
        public static void calculateLevelMark()
        {
            List<Module> l4_List = GlobalVariableHolder.course.getLevel_4().getModuleArray();
            if (l4_List == null)
            {
                GlobalVariableHolder.course.getLevel_4().setLevelMark(0);
            }
            else
            {
                int total = 0;
                int moduleNumber = 0;
                foreach (var currentModule in l4_List)
                {
                    total = total + currentModule.getModuleMark();
                    moduleNumber++;
                }
                GlobalVariableHolder.course.getLevel_4().setLevelMark( (int) (total / (moduleNumber * 1.0)));
            }

            List<Module> l5_List = GlobalVariableHolder.course.getLevel_5().getModuleArray();
            if (l5_List == null)
            {
                GlobalVariableHolder.course.getLevel_5().setLevelMark(0);
            }
            else
            {
                int total = 0;
                int moduleNumber = 0;
                foreach (var currentModule in l5_List)
                {
                    total = total + currentModule.getModuleMark();
                    moduleNumber++;
                }
                GlobalVariableHolder.course.getLevel_5().setLevelMark((int)(total / (moduleNumber * 1.0)));
            }

            List<Module> l6_List = GlobalVariableHolder.course.getLevel_6().getModuleArray();
            if (l6_List == null)
            {
                GlobalVariableHolder.course.getLevel_6().setLevelMark(0);
            }
            else
            {
                int total = 0;
                int moduleNumber = 0;
                foreach (var currentModule in l6_List)
                {
                    total = total + currentModule.getModuleMark();
                    moduleNumber++;
                }
                GlobalVariableHolder.course.getLevel_6().setLevelMark((int)(total / (moduleNumber * 1.0)));
            }
        }

        // Creating a new course and setting the course name and student name
        public static void buildCourse(String studentName, String courseName)
        {
            GlobalVariableHolder.course = new Course(studentName.ToUpper(), courseName.ToUpper());
        }

        // Getting the module objects of a level
        public static List<String> getModuleNameList(String level)
        {
            List<Module> moduleList;
            List<String> moduleNameList = new List<string>();
            if (level.Equals("LEVEL 04"))
            {
                moduleList = GlobalVariableHolder.course.getLevel_4().getModuleArray();
                if (moduleList != null)
                {
                    foreach (var currentModule in moduleList)
                    {
                        moduleNameList.Add(currentModule.getModuleName());
                    }
                }
            }
            else if (level.Equals("LEVEL 05"))
            {
                moduleList = GlobalVariableHolder.course.getLevel_5().getModuleArray();
                if (moduleList != null)
                {
                    foreach (var currentModule in moduleList)
                    {
                        moduleNameList.Add(currentModule.getModuleName());
                    }
                }
            }
            else
            {
                moduleList = GlobalVariableHolder.course.getLevel_6().getModuleArray();
                if (moduleList != null)
                {
                    foreach (var currentModule in moduleList)
                    {
                        moduleNameList.Add(currentModule.getModuleName());
                    }
                }
            }

            return moduleNameList;
        }

        // Calculating the course average using predefined methods
        public static void calculateCourseAverage()
        {
            int courseMark = (int) ((GlobalVariableHolder.course.getLevel_4().getLevelMark() + GlobalVariableHolder.course.getLevel_5().getLevelMark() + GlobalVariableHolder.course.getLevel_6().getLevelMark()) / 3.0);
            GlobalVariableHolder.course.setCourseMark(courseMark);
            GlobalVariableHolder.course.setCourseGrade(getCourseGrade(courseMark));
        }

        // Gets the course grade rom the final average mark
        public static String getCourseGrade(int courseMark)
        {
            if (courseMark >= 70)
            {
                return "FIRST CLASS";
            }
            else if (courseMark >= 60)
            {
                return "UPPER-SECOND CLASS";
            }
            else if (courseMark >= 50)
            {
                return "LOWER-SECOND CLASS";
            } if (courseMark >= 40)
            {
                return "THIRD CLASS";
            }          
 
            return "FAIL";
        }
                
        // Removing a module from memory
        public static void removeModule(String levelName, Module module)
        {
            Level moduleLevel = getLevel(levelName);
            moduleLevel.getModuleArray().Remove(module);
        }

        // Getting the color according to the marks
        public static Color getColor(int courseMarks)
        {
            if (courseMarks > 69) {
                return Color.YellowGreen;
            } else if (courseMarks > 39) {
                return Color.Orange;
            } else {
                return Color.Firebrick;
            }
        }
    }
}
