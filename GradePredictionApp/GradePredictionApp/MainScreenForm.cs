﻿using GradePredictionApp.Models;
using GradePredictionApp.UIComponents;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GradePredictionApp
{
    public partial class MainScreenForm : Form
    {
        private TabControl tabUiComponent;
        private LevelTabPage level4;
        private LevelTabPage level5;
        private LevelTabPage level6;
        private StartPage startPage;
        private SummaryTabPage summaryPage;
        

        public MainScreenForm()
        {
            InitializeComponent();
            GlobalVariableHolder.course = new Course("ANONYMOUS", "MY COURSE");
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            createUiComponents();

            // Retrieves if previosuly stored data is available
            retrieveFromDB();
        }

        private void createUiComponents()
        {
            tabUiComponent = new TabControl();
            tabUiComponent.Size = this.Size;           

            // Create tab pages
            startPage = new StartPage(this);
            startPage.setComponentSizes(this.Size);
            level4 = new LevelTabPage("LEVEL 04", this.Size);
            level5 = new LevelTabPage("LEVEL 05", this.Size);
            level6 = new LevelTabPage("LEVEL 06", this.Size);
            summaryPage = new SummaryTabPage(new Size(this.Width - 8, this.Height));
            
            // Adding the main page
            tabUiComponent.TabPages.Add(startPage);       

            // Adding the tab control to the form
            this.Controls.Add(tabUiComponent);

            // Events when tabs get changed
            tabUiComponent.SelectedIndexChanged += delegate
            {
                if (tabUiComponent.SelectedIndex == 1)
                {
                    level4.refresh();
                }
                else if (tabUiComponent.SelectedIndex == 2)
                {
                    level5.refresh();
                }
                else if (tabUiComponent.SelectedIndex == 3)
                {
                    level6.refresh();
                }
                else if (tabUiComponent.SelectedIndex == 4)
                {
                    summaryPage.refreshPage();
                }
            };

        }

        
        private void retrieveFromDB()
        {
            if (XMLManager.Read())
            {
                // Insert the newly found data into the UI
                level4.refresh();
                level5.refresh();
                level6.refresh();
                summaryPage.refreshPage();

                // Add the remaining pages to the tab control
                tabUiComponent.TabPages.Add(level4);
                tabUiComponent.TabPages.Add(level5);
                tabUiComponent.TabPages.Add(level6);
                tabUiComponent.TabPages.Add(summaryPage);

                MessageBox.Show("Loaded data from file!!!", "Data found");
            }
            else
            {
                MessageBox.Show("Program data not found!!!", "Data not found");
            }

        }

        // Changes to be done when resizing occurs
        private void MainScreenForm_Resize(object sender, EventArgs e)
        {
            try
            {
                if (tabUiComponent != null)
                {
                    tabUiComponent.Size = this.Size;
                    startPage.setComponentSizes(this.Size);
                    level4.setComponentSizes(this.Size);
                    level5.setComponentSizes(this.Size);
                    level6.setComponentSizes(this.Size);
                    summaryPage.setComponentSizes(this.Size);
                }               
            }
            catch (Exception ex)
            {               
            }
        }

        // Display all panels when a new course is created
        public void displayAllPanels()
        {            
            tabUiComponent.TabPages.Add(level4);
            tabUiComponent.TabPages.Add(level5);
            tabUiComponent.TabPages.Add(level6);
            tabUiComponent.TabPages.Add(summaryPage);
        }
    }
}
